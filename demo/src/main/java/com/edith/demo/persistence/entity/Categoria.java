package com.edith.demo.persistence.entity;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name="clientes")
public class Categoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_categoria")
    private Integer idCategoria;
    private String descripcion;
    private Boolean estado;

    @OneToMany(mappedBy = "categoria")
    private List<Producto> productos;
}