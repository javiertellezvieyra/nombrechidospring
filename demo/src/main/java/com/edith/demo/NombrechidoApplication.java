package com.edith.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NombrechidoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NombrechidoApplication.class, args);
	}

}
